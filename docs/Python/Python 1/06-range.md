# 🦾 Fonctions natives

Une présentation rapide de quelques fonctions.

## La fonction `print`

Traditionnellement présentée avec le premier programme

```python
print("Hello World!")      # _in english_

print("Bonjour à tous !")  # en français
```

`#!python print` permet d'afficher un message, **et** de sauter à la ligne ensuite.


!!! info "`print` à plusieurs paramètres"
    On peut afficher plusieurs objets **séparés par une espace** avec un seul `print`, chaque objet est converti en chaine de caractères avant affichage.

    ```pycon
    >>> n = 42
    >>> x = 3.7
    >>> print("Un entier", n, "puis un flottant", x)
    Un entier 42 puis un flottant 3.7
    >>>
    ```


## `abs`, pour la valeur absolue

```pycon
>>> abs(42)
42
>>> abs(-1337)
1337
>>> abs(-0.12)
0.12
```

??? danger "Pour les nombres complexes, en maths expertes"
     `abs` renvoie le module d'un nombre complexe

    ```pycon
    >>> abs(1 - 2j)
    2.23606797749979
    >>>
    ```

!!! abstract "Utilisation"
    Pour calculer un écart `delta` entre deux nombres, qui est un nombre positif, il y a deux façons d'écrire

    === "Avec abs"

        Soustraire l'un par l'autre et renvoyer la valeur absolue.

        ```python
        x = ...  # un nombre inconnu
        y = ...  # un nombre inconnu

        delta = abs(x - y)  # l'écart entre x et y
        ```

        C'est plus lisible.

    === "Avec une conditionnelle"

        Soustraire le plus petit du plus grand.

        ```python
        x = ...  # un nombre inconnu
        y = ...  # un nombre inconnu

        if x < y:
            delta = y - x
        else:
            delta = x - y
        ```

        Il fallait savoir lequel est le plus grand.


## `round`, à un paramètre

La fonction `round(nombre)` renvoie l'arrondi de `nombre` avec le type entier.

```pycon
>>> round(1/3 + 2/3)
1
>>> round(22 / 7)
3
```

!!! warning "Et en cas d'équidistance ?"

    ```pycon
    >>> round(3.5)
    4
    >>> round(4.5)
    4
    >>> round(5.5)
    6
    >>> round(6.5)
    6
    >>>
    ```

    En cas d'équidistance, l'arrondi se fait sur **l'entier pair le plus proche**.

## `round`, à deux paramètres

`#!python round(x, n)` renvoie l'arrondi de $x$ à $10^{-n}$ près.

Deux utilisations courantes.

### Afficher un résultat avec un nombre contenu de chiffres

{{ IDEv('./scripts/round12') }}

On peut aussi utiliser `n` négatif, par exemple avec `n = -3`, on a un arrondi à $10^{-(-3)}$, donc à $1000$ près.


### Tester une presque égalité entre deux flottants

!!! warning "Pas de tests d'égalité entre flottants"

    ```pycon
    >>> 1/3 + 2/3 == 3/3
    True
    >>> 1/10 + 2/10 == 3/10
    False
    ```

    La raison étant que le stockage de $\frac1{10}$, $\frac2{10}$ et $\frac3{10}$ conduit à des approximations en nombres flottants qui ne sont pas égales aux décimaux d'origine. Les arrondis sur les opérations peuvent conduire à un écart qui donne un flottant plus ou moins « voisin ».


Plusieurs méthodes pour tester si deux flottants sont proches

=== "sans `round`"

    ```python
    x = ...  # un nombre inconnu
    y = ...  # un nombre inconnu

    if abs(x - y) < 10**-9:
        print("Les nombres x et y sont proches")
    ```

    Voilà une méthode correcte.

=== "avec un `round`"

    ```python
    x = ...  # un nombre inconnu
    y = ...  # un nombre inconnu

    if round(abs(x - y), 9) == 0:
        print("Les nombres x et y sont proches")
    ```

    On préfère ne faire aucun arrondi si c'est possible.

=== "avec deux `round`"

    ```python
    x = ...  # un nombre inconnu
    y = ...  # un nombre inconnu

    if round(x, 9) == round(y, 9):
        print("Les nombres x et y sont proches")
    ```

    On n'aime pas faire deux arrondis, quand un seul suffit.

!!! savoir "La méthode"
    Ici, la méthode à retenir c'est :
    
    - les flottants sont proches quand « l'écart absolu entre les deux flottants est faible ».


## 🏬 La fonction `range`

La fonction `range` est très utilisée dans les boucles, elle renvoie **un à un**, des termes d'une suite arithmétique.

On peut découvrir ici la fonction native `range` sans écrire de boucle.

Pour cela, on va convertir le résultat de `range` en `tuple`.

### Plusieurs paramètres possibles

#### `range` à un paramètre

- `range(n)` renvoie les entiers de $0$ **inclus** à $n$ **exclu**.

```pycon
>>> tuple(range(5))
(0, 1, 2, 3, 4)
>>>
```

#### `range` à deux paramètres

- `range(n, m)` renvoie les entiers de $n$ **inclus** à $m$ **exclu**.

```pycon
>>> tuple(range(5, 10))
(5, 6, 7, 8, 9)
>>>
```

:warning: **Info**, pour un `tuple` à un élément, il y a une virgule après l'élément. Cela permet de différencier `(5)` qui est un entier, de `(5,)` qui est un tuple à un élément. Techniquement, pour tout `tuple` **non vide**, on peut l'écrire avec une virgule finale superfétatoire comme `(5, 6, 7) = (5, 6, 7,)`.

```pycon
>>> tuple(range(5, 6))
(5,)
>>>
```

:warning: Si $n\geqslant m$, rien n'est renvoyé.

```pycon
>>> tuple(range(8, 8))
()
>>>
```

#### `range` à trois paramètres

- `range(n, m, p)` renvoie les entiers de $n$ **inclus** à $m$ **exclu**, par pas de $p$.

```pycon
>>> tuple(range(5, 10, 2))
(5, 7, 9)
>>> tuple(range(10, 5, -1))
(10, 9, 8, 7, 6)
>>>
```

### Exercices

!!! info
    L'instruction `#!python assert` permet de vérifier une condition écrite juste après.

- Si le test est valide, alors le programme continue sans message d'erreur.
- Si le test est invalide, alors le programme s'arrête et vous avez un message d'erreur.

Votre objectif est d'exécuter les cellules sans aucune erreur.

[Exercice range](https://notebook.basthon.fr/?extensions=admonitions&ipynb=eJy9lUtu2zAQhq8yYDe2QRvWwy8VSVbZB1l00wQ2LU0cIRSl8JHaNXyXLut9bqCLdSjHTVOkrReFBGgkcsh_Pv0kqC1LUUrDks9bVqAVmbCCJdsdb_rndlMhS1gh9ENWflGMM1M6nfq-D3C5Rp3mKRowTsNCC7XCxY26UVc0BtJ78egQvIyTCIlP9OEai0qKFH_mFxcLqIQGpxCehESaKbJ6_-iExYGfcbmu96mzNEOKoxoHI5QB1JrGD7zyOfR616KqUPZ6kPihla73BlWKkFEV6yqJCw55mnNAY6leTuULVBbqb7B0Fqp6n4lVufL9pHnuRT-VzoCzucwNavx6_MhD-RdRsLr-bmCZk1b9bAdsx994yax2xmLGkjshDf5mbVpm-L6tENwoYaiuhaZQp6ndueh24ewMOkMOAYeQQ8Qh5jDqkgqukazKSzVPS6csS5STkrPS2cpZv8i3_4sthI8HKnjKM_wb6KGrXbrofSAOR_OCCbk3pXtGFpKTYdAuYPwvwDjk7RKN_kj0i2tDb5UPkQ-xDyMfxj5MhsN2kcenIIdRPBrTQocTuugZhcPZtF3OySmcDRsH2o8jemsXcHoCYNwcNGFz6LS8zrMT8PoB7cC-d7AfjH2Ynch4y98wPqBWKE2FqW8pUXiWamPvSxWRYJYb-n1t5i-JqyYBPiMJyonV63C2oy9Uy7tSF4Iqx6-NeZGrUrMk3P0A93N9gw)

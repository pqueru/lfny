# Exemple d'utilisation d'un notebook Jupyter

- Dans un carnet, une cellule d'entrée (_input_) commence par `In [ ]:` ou `Entrée[ ]:`
- Entrer une suite d'opérations mathématiques dans la cellule ci-dessous.
    - Par exemple `1 + 2 * 3**4`
- Appuyer sur ++shift+return++ ; vous aurez le résultat (_output_), dans `Out[ ]:` ou `Sortie[ ]:`
- Il est aussi possible de cliquer sur :fontawesome-solid-forward-step: Exécuter
- Vous pouvez tester d'autres opérations.

{{ python_carnet('scripts/entier.ipynb') }}

## Exercices

{{ python_carnet('scripts/division.ipynb') }}
# 🩺 Fonctions - Exercices

## Boucle `for`

Entrer dans la cellule et appuyer sur ++shift+return++

{{ python_carnet('./scripts/puissance_tutor.ipynb', hauteur=800)}}

- Essayer avec d'autres valeurs et observer le comportement de `#! assert`
- Remarquer le changement de valeur de la variable `_`.

## Boucle `while`

```python
def mystère(n):
    if n < 0:
        n = -n
    while n > 10:
        n = n // 10
    return n
```

- Tester à la main, `mystère(-58402)`.
- Avons-nous déjà croisé une telle fonction ?

## La fonction `range`

On rappelle qu'on peut utiliser `range` avec un, deux ou trois paramètres.

!!! info "Boucle `for tour in range...`"
    La variable de boucle `tour` (ou un autre nom) peut être utilisée.

    À chaque tour, la variable prend la valeur suivante que `range` renvoie.


=== "`range` à 1 paramètre"

    ```python
    for i in range(5):
        print(10 * i)
    ```

    ```output
    0
    10
    20
    30
    40
    ```
    
    Il y a eu 5 tours de boucle, et la valeur de `i` a été successivement 0, 1, 2, 3, 4.

=== "`range` à 2 paramètres"

    ```python
    somme = 0
    for i in range(500, 600):
        somme = somme + i
    print(somme)
    ```

    ```output
    54950
    ```

    On a calculé la somme des entiers de $500$ **inclus** à $600$ **exclu**. Il y en a $600-500 = 100$, et la somme vaut $54\,950$.

=== "`range` à 3 paramètres"

    ```python
    produit = 1
    for i in range(20, 100, 7):
        produit = produit * i
    print(produit)
    ```

    ```output
    468173867972329728000
    ```

    On a calculé le produit des entiers de $20$ **inclus** à $100$ **exclu**, par pas de $7$.

    $$20×27×34×41×48×55×62×69×76×83×90×97$$


## Exercices

### Avec des tuple

Compléter le code pour qu'à la fin le point $M$ soit
l'image de $P$ par une translation de vecteur $\vec v$.

Version avec une fonction :

{{ IDE('./scripts/vecteurs_2') }}


!!! info "docstring"
    Dans la fonction précédente, il y a 6 lignes qui **ne sont pas des instructions**, c'est du texte informatif ; une _docstring_. C'est une excellente pratique à avoir : on documentera désormais toutes les fonctions. On peut même documenter un exemple d'utilisation...

    ```python
    """Renvoie le `point_M` : 
    la translation de `point_P` par `vecteur _v`
    
    >>> translation((7, 8), (-2, 5))
    (5, 13)
    
    """
    ```


### Somme des carrés

Créer une fonction qui renvoie la somme des carrés des entiers de $1$ à $n$.

- Avec une boucle `#! for`
- On aura par exemple un résultat $14$, pour $n=3$.

{{ IDE() }}

??? tip "Indices et solution"
    1. De $1$ à $n$, il y a $n$ entiers, donc on peut faire une boucle de $n$ tours.
    2. On peut utiliser un indice qui commence à $1$, et qui finit à (???) **exclu**.
    3. On peut utiliser une variable `resultat` qui accumule les carrés.

    ??? done "Solution"

        ```python
        def somme_carres(n):
            """Renvoie la somme des carrés des entiers
            de 1 à n inclus.

            >>> somme_carres(3) == 1 + 4 + 9
            True

            """
            resultat = 0
            for i in range(1, n+1):
                resultat = resultat + i*i
            return resultat
        ```

### Puissance ou pas

Créer une fonction `est_puissance(n, a)` qui renvoie
 `#!python True` ou `#!python False` selon
 que `n` est une puissance de `a`.

```pycon
>>> est_puissance(125, 5)
True
>>> est_puissance(250, 5)
False
```

{{ IDE() }}

??? tip "Indication et solution"
    Tant que $n$ est divisible par $a$, on divise $n$ par $a$.
    ??? done "Solution"

        ```python
        def est_puissance(n, a):
            """Renvoie un booléen, la réponse à :
            `n` est-il une puissance de `a` ?
            """
            if a == 0:
                return n == 0
            while n % a == 0:
                n = n // a
            return n == 1  # c'est bien un booléen, et la réponse
        ```

        1. Il ne faut pas oublier le cas $a = 0$, qui pourrait provoquer une erreur de division par zéro. Dans ce cas, seul $n=0$ est une puissance de $0$.
        2. Ensuite, on divise autant que possible par $a$, et si le résultat est égal à $1$, $n$ **était** une puissance de $a$.


### Une grande puissance :boom:

Créer une fonction qui renvoie la plus grande puissance de $a$ qui a **moins** de $k$ chiffres.

- Avec une boucle `#! while`
- On aura par exemple un résultat $729$, pour $a=3$ et $k=4$

{{ IDE() }}

??? tip "Indices et solution"
    1. On initialise une `puissance` à $a^0 = 1$.
    2. On fait une boucle `#! while` en faisant une comparaison avec $10^k$.
    3. Attention, $10^k$ possède $k+1$ chiffres !
    3. On fera attention au choix de la comparaison et des cas d'égalité.
    3. On vérifiera avec $a = 10$ et $k = 3$.

    ??? done "Solution"

        ```python
        def grande_puissance(a, k):
            """Renvoie la plus grande puissance de `a`
            - qui a moins de `k` chiffres.
            """
            puissance = 1
            while puissance < 10 ** k:
                puissance = puissance * a
            # `puissance` a au moins `k` chiffres,
            #  on revient en arrière un coup.
            puissance = puissance // a
            return puissance
        ```

### Itérer une fonction plusieurs fois :boom:

On considère la fonction $f$ définie sur $\mathbb N$ par
$f(n) = \begin{cases}3n+1 & \text{ si } n \text{ est impair,}\\ \dfrac{n}2 & \text{ si } n \text{ est pair.}\end{cases}$

On aimerait calculer $f(f(f(f(\dots f(n) \dots))))$ pour un entier $n$ et un nombre d'appels imbriqués à $f$ égale à $k$.

{{ IDE() }}

1. Écrire une fonction python `f` dans un script, qui correspond à la fonction mathématique $f$.
2. Écrire ensuite une fonction `imbrique(k, f, n)` qui imbrique `k` appels successifs à `f`, en partant de `n`.
3. Vérifier les tests suivants.

```pycon
>>> imbrique(1, f, 5)
16
>>> imbrique(2, f, 5)
8
>>> imbrique(7, f, 7)
13
>>> imbrique(16, f, 7)
1
```

??? done "Solution"
    === "Avec une boucle `#!python for`"

        ```python
        def f(n):
            if n % 2 == 1:
                return 3*n + 1
            else:
                return n // 2
        
        def imbrique(k, f, n):
            resultat = n
            for _ in range(k):
                resultat = f(resultat)
            return resultat
        ```


### Défi :boom:

!!! tip "Nombre d'écritures - 1"
    Comptons le nombre de façons d'écrire un entier $n$
    comme somme d'entiers.

    === "Cas $n=3$"

        - $3 = 2 + 1$
        - $3 = 1 + 1 + 1$
        - $3 = 1 + 2$

        Il y a $3$ façons d'écrire $3$ comme une somme.

    === "Cas $n=4$"

        - $4 = 1 + 2 + 1$
        - $4 = 2 + 2$
        - $4 = 1 + 1 + 2$
        - $4 = 2 + 1 + 1$
        - $4 = 1 + 3$
        - $4 = 1 + 1 + 1 + 1$
        - $4 = 3 + 1$

        Il y a $7$ façons d'écrire $4$ comme une somme.
    
    :boom: Et pour $n = 42$ ? :boom:

    ??? Done "Solution"
        $n$ s'écrit $1+1+1\cdots+1+1+1+1$. Chaque $+$ peut se voir comme une barrière que l'on place ou non. On obtient toutes les écritures, et en plus $n=n$ quand toutes les barrières sont ouvertes. Ce n'est pas une somme, nous ne la comptons pas. Il y a $n-1$ signe $+$, chacun à deux possibilités.

        Il y a $2^{n-1}-1$ écritures de $n$ comme somme d'entiers (en comptant l'ordre).

### Défi difficile :boom: :boom: :boom:

!!! danger "Nombre d'écritures - 2"
    Comptons le nombre de façons d'écrire un entier $n$
    comme somme d'entiers, l'ordre de la somme ne compte pas.

    === "Cas $n=3$"

        - $3 = 2 + 1$
        - $3 = 1 + 1 + 1$

        Il y a $2$ façons d'écrire $3$ comme une somme.

    === "Cas $n=5$"

        - $5 = 4 + 1$
        - $5 = 3 + 2$
        - $5 = 3 + 1 + 1$
        - $5 = 2 + 2 + 1$
        - $5 = 2 + 1 + 1 + 1$
        - $5 = 1 + 1 + 1 + 1 + 1$

        Il y a $6$ façons d'écrire $5$ comme une somme.

    :boom: :boom: :boom: Et pour $n = 42$ ? :boom: :boom: :boom:

    ??? tip "Indices et solution"
        1. C'est un vrai défi, vous savez tout ce qui est nécessaire en Python pour réussir.
        2. Pour $n = 10$, la réponse est $41$ :wink:.
        3. Utiliser une fonction à **deux** paramètres sera utile.

        ??? done "Solution"
            On trouve qu'il y a $53\,173$ façons d'écrire $42$
            comme somme d'entiers, sans compter l'ordre.

            À vous de trouver le plus joli code.

            Et si on exige des termes distincts dans la somme ? C'est un autre problème, qui n'est pas plus dur.

def somme_chiffres(n

Renvoie la somme des chiffres de "n"
... # À corriger et compléter










#-----------------------------------
# TESTS ; appuyer sur Exécuter
# Ne pas modifier ci-dessous
#-----------------------------------

assert somme_chiffres(1337) == 14
assert somme_chiffres(121) == 4
assert somme_chiffres(4) == 4
assert somme_chiffres(-121) == 4
assert somme_chiffres(999111555) == 45
print("La fonction `somme_chiffres` semble correcte.")

# 22/7, une fraction proche de pi
print("Un arrondi à 5 chiffres de 22/7 est", round(22/7, 5))


# e, la base des logarithmes naturels
# e_6 est une bonne valeur approchée de e
e_6 = 1 + 1 + 1/2 + 1/6 + 1/24 + 1/120 + 1/720
print("À 10^-3, e est environ égal à", round(e_6, 3))


# x = 123456789.0123456789
# x est un grand nombre ; trop précis
print("À 1000 près, x est égal à", round(x, -3))

# 🔍 Puzzles - Entraînement sur les listes (Niveau 1re)
Ces puzzles sont issus du site de [Romain Janvier](http://nsi.janviercommelemois.fr/).

## Position
=== "Version difficile"
    <iframe id="frame1" src="https://www.codepuzzle.io/iframe/E34S" width="100%" height="600" frameborder="0" loading="lazy"></iframe>

=== "Version moyenne"
    <iframe id="frame1" src="https://www.codepuzzle.io/iframe/67ZE" width="100%" height="600" frameborder="0" loading="lazy"></iframe>

=== "Version facile"
    <iframe id="frame1" src="https://www.codepuzzle.io/iframe/R6EH" width="400" height="600" frameborder="0" loading="lazy"></iframe>

## Compter
=== "Version difficile"
    <iframe id="frame2" src="https://www.codepuzzle.io/iframe/V3AP" width="100%" height="600" frameborder="0" loading="lazy"></iframe>

=== "Version moyenne"
    <iframe id="frame2" src="https://www.codepuzzle.io/iframe/NTMZ" width="100%" height="600" frameborder="0" loading="lazy"></iframe>

=== "Version facile"
    <iframe id="frame2" src="https://www.codepuzzle.io/iframe/ADSR" width="400" height="600" frameborder="0" loading="lazy"></iframe>

## Maximum
=== "Version difficile"
    <iframe src="https://www.codepuzzle.io/iframe/UJQY" width="100%" height="600" frameborder="0"></iframe>

=== "Version moyenne"
    <iframe src="https://www.codepuzzle.io/iframe/5ZEM" width="100%" height="600" frameborder="0"></iframe>

=== "Version facile"
    <iframe id="frame3" src="https://www.codepuzzle.io/iframe/D4W2" width="400" height="600" frameborder="0" loading="lazy"></iframe>
    <iframe id="frame4" src="https://www.codepuzzle.io/iframe/C4HG" width="400" height="600" frameborder="0" loading="lazy"></iframe>

## Moyenne
=== "Version difficile"
    <iframe id="frame5" src="https://www.codepuzzle.io/iframe/3BKJ" width="100%" height="600" frameborder="0" loading="lazy"></iframe>

=== "Version moyenne"
    <iframe id="frame5" src="https://www.codepuzzle.io/iframe/TDY3" width="100%" height="600" frameborder="0" loading="lazy"></iframe>

=== "Version facile"
    <iframe id="frame5" src="https://www.codepuzzle.io/iframe/S97Y" width="400" height="600" frameborder="0" loading="lazy"></iframe>
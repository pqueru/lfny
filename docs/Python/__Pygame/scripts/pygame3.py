from random import randint
import pygame
import time
 
 
SCREEN_WIDTH = 910
SCREEN_HEIGHT = 603

NOIR = (0, 0, 0)
BLANC = (255, 255, 255)
ROUGE = (255, 0, 0)
ROUGE2 = (190, 25, 25)
BLEU = (27, 24, 181)

VITESSE_MISSILE = 15
 
running = True
 
pygame.init()

myfont = pygame.font.SysFont('Comic Sans MS', 30)
MESSAGE_PERDU = myfont.render('GAME OVER!', True, ROUGE2)

# Création de la fenêtre
SCREEN = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
# Affichage de l'arrière plan
FOND = pygame.image.load('background.jpg').convert()
IMG = pygame.image.load("pyg.png")

class Joueur:
    def __init__(self, pos):
        self.image = pygame.image.load('xwing.png')
        self.position = [pos[0], pos[1]]
        self.vitesse = 10
        self.score = 0
        
    def get_position(self):
        return (self.position[0], self.position[1])
    
    def update(self):
        SCREEN.blit(self.image, self.get_position())
        
class Alien():
    def __init__(self):
        self.image = pygame.image.load('space11.png')
        self.position = [randint(0, SCREEN_WIDTH), 0]
        self.vitesse = 1
        
    def get_position(self):
        return (self.position[0], self.position[1])
    
    def update(self):
        if 0 <= self.position[1] < SCREEN_HEIGHT:
            self.position[1] += self.vitesse
            
        SCREEN.blit(self.image, self.get_position())

class Missile:
    cote = True
    def __init__(self, pos):
        self.image = pygame.image.load('lazer1.png')
         
        self.position = self.image.get_rect()
        self.position = [pos[0], pos[1]]
        if Missile.cote:
            self.position[0] += 40
        Missile.cote = not Missile.cote
        
    def get_position(self):
        return (self.position[0], self.position[1])
    
    def update(self):
        if self.position[1] >= -10:
            self.position[1] -= VITESSE_MISSILE
            
        SCREEN.blit(self.image, self.get_position())



def distance(vaisseau1, vaisseau2):
    return (vaisseau1.position[0]-vaisseau2.position[0])**2 + \
            (vaisseau1.position[1]-vaisseau2.position[1])**2
    
class Jeu:
    def __init__(self):
        self.temps = 0
        self.joueur = Joueur((400, 450))
        self.clock = pygame.time.Clock()
        self.liste_missiles = []
        self.liste_aliens = []
        
    def main_events(self):
        global running, temps
        
        SCREEN.blit(FOND, (0,0))
        
        for event in pygame.event.get():
            
            if event.type == pygame.QUIT:
                running = False
           
        self.clock.tick(20)
        self.temps += 1
        
        if (self.temps == 10) or (self.temps % 80 == 0):
            self.liste_aliens.append(Alien())
        
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_LEFT]:
            self.joueur.position[0] -= self.joueur.vitesse
        elif pressed[pygame.K_RIGHT] and self.joueur.position[0] > 0:
            self.joueur.position[0] += self.joueur.vitesse
        elif pressed[pygame.K_UP] and self.joueur.position[0] < SCREEN_WIDTH:
            self.joueur.position[1] -= self.joueur.vitesse
        elif pressed[pygame.K_DOWN] and self.joueur.position[1] > 0:
            self.joueur.position[1] += self.joueur.vitesse
        elif pressed[pygame.K_SPACE] and self.joueur.position[1] < SCREEN_HEIGHT:
            #print(len(self.liste_missiles))
            self.liste_missiles.append(Missile(self.joueur.get_position()))
        
    def detect_collision(self):
        for missile in self.liste_missiles:
            for alien in self.liste_aliens:
                if distance(missile, alien) < 400:
                    self.joueur.score += 10
                    explosion_position = [alien.position[0] + 20, alien.position[1] + 20]
                    self.liste_aliens.remove(alien)
                    self.liste_missiles.remove(missile)
                    explosion = pygame.image.load('Bubble.png')
                    pygame.draw.circle(SCREEN, ROUGE, explosion_position, 15, 15)

    def affiche_score(self):
        texte_score = 'Score : ' + str(self.joueur.score)
        message_score = myfont.render(texte_score, True, BLEU)
        SCREEN.blit(message_score, (10,10))
        print(texte_score)
        
    
    def update(self):
        SCREEN.blit(FOND, (0,0))
        SCREEN.blit(IMG, (350,50))
        
        SCREEN.blit(self.joueur.image, self.joueur.get_position())
            
        for m in self.liste_missiles:
            m.update()
        for a in self.liste_aliens:
            a.update()
            
        self.detect_collision()
        
        self.test_fin()
        
        self.affiche_score()
        
        pygame.display.update()
        
    
    def test_fin(self):
        for a in self.liste_aliens:
            if distance(a, self.joueur) < 400:
                running = False
                SCREEN.blit(MESSAGE_PERDU, (400,300))
                pygame.display.update()
                time.sleep(5)


### Début du jeu
jeu1 = Jeu()

# SCREEN.fill(BLANC)
# 
# MESSAGE_BIENVENUE = myfont.render('Bienvenue !', True, BLEU)
# MESSAGE_NOM = myfont.render('Quel est votre nom ?', True, BLEU)
# SCREEN.blit(MESSAGE_BIENVENUE, (200,200))
# SCREEN.blit(MESSAGE_NOM, (200,250))
# pygame.display.update()
# nom = ''
# print("wfef")
# 
# letters = {x: pygame.key.key_code(x) for x in "abcdefghijklmnopqrstuvwxyz"}
# print("apres")
# 
# clock = pygame.time.Clock()
# clock.tick()
# 
# attendre = True
# while attendre:
#     for event in pygame.event.get():
#         if event.type == pygame.QUIT:
#             attendre = False
#     
#     for event in pygame.event.get():
#         if event.type == pygame.KEYDOWN:
#             touche = event.key
#             if touche == pygame.K_RETURN:
#                 attendre = False
# 
#             for (l, value) in letters.items() :
#                 if touche == value:
#                     nom += l
#                     message_nom = myfont.render(nom, True, BLEU)
#                     SCREEN.blit(message_nom, (200,300))
#                     pygame.display.flip()

while running:
    jeu1.main_events()

    jeu1.update()


pygame.quit()
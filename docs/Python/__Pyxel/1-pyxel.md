# Pyxel
> `Pyxel` est un moteur de jeu vidéo rétro pour Python, libre et open-source.  
La documentation officielle de Pyxel : [https://github.com/kitao/pyxel](https://github.com/kitao/pyxel)

## Installation
Dans un terminal, saisissez la ligne suivante :
```terminal
pip install pyxel
```

## Premiers pas

### Principe général d'un jeu vidéo avec une classe
Une **boucle** fait progresser le jeu.  
A chaque tour :

1. On écoute les interactions du joueur  
2. On met à jour l'état du jeu 👉 `update()`  
3. On dessine les éléments à l'écran 👉 `draw()`  
4. On attend quelques millisecondes

Par convention, on écrit les constantes en majuscule : WIDTH, HEIGHT, CASE.  
```python
import pyxel

# constantes du jeu
TITLE = "mon jeu"
WIDTH = 128
HEIGHT = 128
CASE = 4
```

On crée une classe pour le jeu :  
À la création d'un objet de la classe,  
1. on crée la fenêtre du jeu : `pyxel.init(WIDTH, HEIGHT, title=TITLE)`  
2. on initialise les éléments du jeu  
2. on lance l’exécution du jeu avec `pyxel.run(self.update, self.draw)` qui fait appel aux deux méthodes `update()` et `draw()`, qui seront appelées 30 fois par seconde.  

La structure générale est ainsi :
```python
import pyxel

# constantes du jeu
TITLE = "mon jeu"
WIDTH = 128
HEIGHT = 128
CASE = 4


class Jeu:
    def __init__(self):
        
        # taille de la fenetre 128x128 pixels
        pyxel.init(WIDTH, HEIGHT, title=TITLE)
        
        # initialisation des éléments du jeu
        

        # excécution du jeu
        pyxel.run(self.update, self.draw)
    
    # =====================================================
    # == UPDATE
    # =====================================================
    def update(self):
        """mise à jour des variables (30 fois par seconde)"""
        

        # On termine le jeu en appuyant sur la touche Q
        if pyxel.btnp(pyxel.KEY_Q):
            pyxel.quit()
            
    # =====================================================
    # == DRAW
    # =====================================================
    def draw(self):
        """création et positionnement des objets (30 fois par seconde)"""

        # on efface l'écran et on le remplit de noir
        pyxel.cls(0)
        
        # on dessine les éléments du jeu

# On crée une instance du jeu
Jeu()
```


???+ note dépliée

    Il existe de nombreuses méthodes toutes faites permettant de dessiner, écrire du texte ...  

!!! warning "Les couleurs"

    Les couleurs sont désignées par des entiers de 0 à 15 (0 désignant le noir.)

    ![](images/05_color_palette.png){ width=35% }

    ![](images/pyxel_palette.png){ width=35% }

!!! info "Quelques méthodes"

    | Action | Instruction |
    | :--- | : --- |
    | Effacer l'écran et le remplir de noir | `pyxel.cls(0)` |  
    | Détection d’interactions utilisateurs | Flèches clavier `pyxel.btn(pyxel.KEY_RIGHT)` ou UP, LEFT, DOWN  et barre espace : `pyxel.btn(pyxel.KEY_SPACE)` |
    | Écrire du texte | `pyxel.text(50, 64, 'GAME OVER', 7)` |
    | Dessiner un rectangle | `pyxel.rect(x, y, long, larg, col)` `x` et `y` : coordonnées du sommet en haut à gauche ; `long` et `larg` : les dimensions ; `col` le numéro de la couleur |


### Création d'un petit jeu : Snake 🐍
Principe :  
- le serpent se déplace automatiquement, on peut changer sa direction avec les flèches du clavier.
- s'il mange la pomme, il grandit et celle-ci réapparait dans une case vide
- s'il quitte l'écran ou se mord, il meurt, et le jeu s’arrête.

#### dessiner le serpent
#### La grille  
Les cases sont repérées par leurs coordonnées. L’origine est en haut à gauche. On commence à zéro, la 1ère coordonnée est l’abscisse (numéro de colonne) et la seconde l’ordonnée (numéro de ligne)

Le serpent est représenté par une double liste : `snake = [[3,3],[2,3],[1,3]]`, définie au début du programme (après `pyxel.init()`)

Le premier élément est sa tête, elle est en `[3,3]` ensuite vient son corps.

Dessiner le serpent  
Pour dessiner sur l’écran les cases du serpent, on utilise la méthode `pyxel.rect(x, y, L, l, color)`  
• `x` et `y` sont les coordonnées du coin supérieur gauche, `L` et `l` les dimensions du rectangle.  
• `color` est un indice entre 0 et 15 désignant une couleur de la palette prédéfinie Pyxel.  

Les instructions suivantes seront placées dans la méthode `draw()`

```python
# dessiner la tête en orange (couleur 9)
x_head, y_head = snake[0]
pyxel.rect(x_head * CASE, y_head * CASE, CASE, CASE, 9)

# dessiner le corps en vert (couleur 11)
for anneau in snake[1:]:
    x, y = anneau[0], anneau[1]
    pyxel.rect(x * CASE, y * CASE, CASE, CASE, 11)
```

#### 💯 Affichage du score
```python
# affichage du score
pyxel.text(2, 2, f"SCORE : {self.score}", 7)
```

#### 🏃🏻 Animation du serpent
```python
# direction du déplacement
direction = [1, 0]

# coordonnées de la nouvelle tête
head = [self.snake[0][0] + direction[0], self.snake[0][1] + direction[1]]

# mise à jour de l'attribut snake
self.snake.insert(0, head)
self.snake.pop(-1)
```

Utiliser une pile...

Le problème est que cela va beaucoup trop vite...
Pour régler ce problème, on peut utiliser le compteur de frames intégré à Pyxel, en effectuant le mouvement par exemple tous les 15 frames.  
Pour cela, on peut définir la constante `FRAME_REFRESH = 15` et mettre le mouvement au sein d'un test :  
```python
if pyxel.frame_count % FRAME_REFRESH == 0:
    # coordonnées de la nouvelle tête
    head = [self.snake[0][0] + direction[0], slef.snake[0][1] + direction[1]]

    # mise à jour de l'attribut snake
    self.snake.insert(0, head)
    self.snake.pop(-1)
```

#### ➡️ Changement de direction du serpent
On rajoute des tests qui modifie la direction si le joueur appuie sur l'une des flèches du clavier.  
```python
if pyxel.btn(pyxel.KEY_ESCAPE):
    pyxel.quit()
elif pyxel.btn(pyxel.KEY_RIGHT):
    direction = [1, 0]

```

Compléter le script pour gérer toutes les directions.

#### ☠️ Mort du serpent
Le serpent meurt lorsqu'il se mord la queue, ou lorsqu'il quitte l'écran. Dans ce cas, le jeu s’arrête, et on quitte la fenêtre.  
Compléter le code pour afficher un message si le serpent meurt.

#### 🍎 Gestion de la pomme
On place une pomme (matérialisée par une case rose) au hasard dans la fenêtre. Lorsque le serpent mange la pomme, il grandit d’un anneau (sa queue n’est pas effacée), et le score augmente de 1.  

Si la pomme est mangée, on augmente le score de 1, et on recrée la pomme ailleurs en faisant attention à ne pas la placer sur le serpent...

Compléter le code.

#### Pour aller plus loin
- Proposer de rejouer.  
- Conserver un high score (tant qu'on ne quitte pas le jeu puis de manière persistante en l'écrivant dans un fichier)  
- Améliorer les graphismes  
- Ajouter du son ...

### Création d'un second jeu : Space Invaders 👾
Dans un premier temps, le vaisseau sera représenté par un carré bleu.

On créera une classe pour le vaisseau et une classe pour les ennemis.

Initialiser la classe `Jeu` :  
- les constantes globales  
- la création du vaisseau 

Écrire la méthode `update(self)` :
- on déplace le vaisseau si le joueur a pressé l'une des flèches du clavier. On écrira une méthode `vaisseau_deplacement(self)`

Écrire la méthode `draw()` :  
- on efface la fenêtre  
- on dessine le vaisseau  


#### Partie 1 : Mise en place du vaisseau
La structure du code ressemblera à :
```python
import pyxel

from random import randint

# constantes du jeu
TITLE = "space invaders"
WIDTH = 128
HEIGHT = 128

class Vaisseau:
    def __init__(self):
        # initialisation du vaisseau
        # coordonnées du point en haut à gauche du vaisseau 
        ...
        
        # taille du carré contenant le vaisseau
        ...
        
        # nombre de vies
        ...
        
    def deplacement(self):
        ...

class Jeu:
    def __init__(self):
        
        # taille de la fenetre 128x128 pixels
        pyxel.init(WIDTH, HEIGHT, title=TITLE)
        
        # initialisation des éléments du jeu
        # création du vaisseau
        ...
        
        # excécution du jeu
        pyxel.run(self.update, self.draw)
        
    
    # =====================================================
    # == UPDATE
    # =====================================================
    def update(self):
        """mise à jour des variables (30 fois par seconde)"""

        # On déplace le vaisseau
        ...

        # On termine le jeu en appuyant sur la touche Escape
        if pyxel.btnp(pyxel.KEY_ESCAPE):
            pyxel.quit()
            
    # =====================================================
    # == DRAW
    # =====================================================
    def draw(self):
        """création et positionnement des objets (30 fois par seconde)"""

        # on efface l'écran et on le remplit de noir
        ...
        
        # on dessine les éléments du jeu
        # dessin du vaisseau
        ...
            
        # affichage du score
        ...
            
Jeu()
```

#### Partie 2 : Gestion des tirs
Nous allons représenter chaque tir par un rectangle jaune, de largeur 1 pixel et de hauteur 4 pixels.  
Au début du jeu, la liste des tirs est vide. On peut la créer comme un attribut de la classe `Jeu`.  
À chaque fois que l’utilisateur appuiera puis relâchera la touche Espace, cela créera un nouveau tir qui sera ajouté à la liste.  
Concrètement, le tir sera stocké en mémoire par les coordonnées de son coin supérieur gauche.

Dans la classe `Jeu`, définir :  
- l'attribut `tirs_liste`  
- la méthode `tir_creation(self)`

Compléter la méthode `update(self)` pour créer un éventuel tir.

Compléter la méthode `draw(self)` pour afficher tous les tirs.

On voudrait maintenant que les différents tirs se déplacent vers le haut au fur et à mesure, pour
créer l’animation. Pour cela, on va compléter la méthode `update(self)` qui appellera une méthode `tirs_deplacement(self)` de la classe `Jeu`.

#### Partie 3 : Gestion des ennemis
Cette partie est très similaire à la précédente avec deux différences cependant : les ennemis apparaîtront à un endroit aléatoire en haut de l'écran, et à intervalles de temps régulier.

Chaque ennemi sera représenté par un carré rose de côté 8 pixels. Au début du jeu, la liste des ennemis sera vide. À intervalles de temps régulier, un nouvel ennemi sera généré et ajouté à la liste. Comme pour les tirs, le carré sera stocké en mémoire par les coordonnées de son coin supérieur gauche.

Pour créer un ennemi par seconde, on pourra utiliser la commande :  
```python
if (pyxel.frame_count % 30 == 0):
```

Créer la liste des ennemis, gérer leurs déplacements et les dessiner.  

#### Partie 4 : Gestion des collisions
Pour gérer les collisions, on va créer deux nouvelles méthodes :   
`ennemis_suppression(self)` et `vaisseau_suppression(self)` dans la classe `Jeu` qui seront toutes les deux appelées par la méthode `update(self)`.

On voudrait maintenant que la destruction d’un ennemi soit matérialisée par une explosion, sous
forme d’un cercle qui grandit progressivement puis disparaît (comme une onde de choc
concentrique)  
Pour cela, on va créer deux nouvelles méthodes : `explosions_creation(self)` et `explosions_animation(self)` comme expliqué ci-dessous.

Chaque explosion sera représentée par une liste :  
- les deux premiers paramètres sont les coordonnées désignant le vaisseau ou l’ennemi qui est touché  
- le troisième paramètre représente le diamètre du cercle d’explosion. On partira d’un
diamètre égal à zéro (il augmentera ensuite lors de l’animation).  

Les explosions sont stockées dans une liste : `explosions_liste` : c’est une liste vide au départ, et la méthode `explosions_creation(self)` se contente d’y ajouter la nouvelle explosion créée.

On crée enfin la méthode `explosions_animation` qui agrandir la cercle jusqu'à un diamètre de 10, avant de disparaître.

Ne pas oublier de mettre à jour la méthode `update` !

Il faut enfin dessiner les explosions dans la méthode `draw`.  
On utilise pour cela la méthode pyxel :  
`circb(x, y, r, col)` : dessine le contour d'un cercle de rayon `r` et de couleur `col` à `(x, y)`

Pour faire plus joli lors de l’animation, les cercles changent de couleur. On alternera, par exemple, les couleurs rose, orange et jaune.

#### Partie 5 : Ajouter des images
Il est possible d’utiliser un utilitaire `Pyxel` pour créer ou éditer des images, ou plus
généralement des ressources (sons ...).  
On lance cet éditeur depuis un terminal.  

On se place dans le répertoire dans lequel se trouve notre jeu puis on exécute l'instruction suivante :  
```terminal
pyxel edit images.pyres
```
On peut évidemment modifier le nom `images`.

L’interface de l’utilitaire `Pyxel` s’ouvre :  
![](images/image_editor.gif){ width=20% }

Cet éditeur permet de dessiner des
images façon pixel art, très simples et 
donc très légères.

On dessine dans la partie gauche, qui « zoome »
sur un carré de $16 \times 16$ pixels découpé en 4 parties.
Ce carré est en fait une petite portion de la
`tilemap` (à droite), sorte d’immense nappe sur
laquelle on va juxtaposer tous les dessins, et qu’on « découpera » en petits morceaux, pour venir coller les morceaux sur la fenêtre de jeu au bon endroit.

On peut au choix :  
- Dessiner soi-même
- Utiliser une tilemap déjà faite, au format `.pyxres`

Dessiner votre vaisseau !

Si on manque d'imagination, on pourra s'inspirer de cette image :  
![](images/spaceinvader_tilemap.png){ width=35% }

Maintenant, nous avons une magnifique `tilemap` avec plusieurs images, mais nous ne les avons pas
encore « blittées » (« collées ») sur l’écran ...

Il faut au préalable que le programme charge la `tilemap` dans la mémoire de l’ordinateur, en
écrivant au début du script, dans le constructeur de la classe `Jeu`, l’instruction : 
```python
pyxel.load("images.pyres")
```

Une fois que votre banque d'images est chargée, vous pouvez afficher n'importe quelle image à l'aide de la méthode `blt()` :  
```python
pyxel.blt(x, y, img, u, v, w, h)
```

Cette méthode copie :  
- à la position `(x, y)` de l’écran de jeu
- l'image de la `tilemap` qui est située à la position `(u, v)` et qui a pour taille `(w, h)`.

Elle « découpe » donc un morceau de la `tilemap` (morceau décrit par `u`, `v`, `w`, `h`) et vient le « coller » au point `(x, y)` sur l’écran.  

Le paramètre `img` correspond à l'une des trois grandes images de la banque d'images. Par défaut sa valeur est zéro (cela correspond à la `tilemap` qu’on voit en ouvrant l’éditeur).

#### Application à notre jeu
Dans le code `Python`, remplacer les dessins de rectangles par des "blittages" d’images.
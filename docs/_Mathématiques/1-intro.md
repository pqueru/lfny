# Accueil

Vous trouverez ici des exercices pour s'entraîner à Python

Pour chaque exercice, vous avez un nombre fini d'essais avant qu'une solution vous soit proposée.

Pour répondre aux exercices, vous avez à votre disposition un IDE comme celui-ci :

{{ IDEv('intro', MAX = 5) }}

Vous pouvez y écrire votre programme et le modifier comme vous voulez.

Pour tester le résultat, vous pouvez cliquer sur le bouton ![](../images/buttons/icons8-play-64.png){width=20}, une console s'ouvrira alors sur la droite ou en dessous, et vous pourrez faire tous les tests voulus.

Pour valider le résultat, il faudra cliquer sur ![](../images/buttons/icons8-check-64.png){width=20}

Si la réponse est juste, vous devriez voir un message de félicitation dans la console.

Sinon, il vous restera 4 (ou plus) validations (5 ou plus en tout!) avant que la réponse ne s'affiche.

Vous pouvez recharger le programme d'origine en cliquant sur ![](../images/buttons/icons8-restart-64.png){width=20}

A vous de jouer !

def seuil(p):
    n = 1
    while 1 - (5/6)**n <= p:
        n = n + 1
    return n
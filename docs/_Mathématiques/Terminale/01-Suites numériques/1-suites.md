# Suites numériques

## <font color=orange>Amérique du Sud - 2022</font>
Soit $u_n$ la suite définie par $u_0 = 4$ et, pour tout entier naturel $n$, $u_{n+1} = \dfrac{1}{5}u_n^2$.

Compléter la fonction ci-dessous écrite en langage Python. Cette fonction est nommée `suite_u` et prend pour
paramètre l'entier naturel $p$.

Elle renvoie la valeur du terme de rang $p$ de la suite $(u_n)$.

{{ IDE('./1-amsud2022/amsud2022') }}

## <font color=orange>Épreuve 0 - 2020</font>
On considère la fonction Python seuil ci-dessous, où $p$ est un nombre réel appartenant à l’intervalle $]0~;~1[$.

Quelle est la valeur renvoyée par la commande `seuil(0.9)` ?

{{ IDE('./1-epreuve0/epreuve0') }}

??? "Solution"    
    - `seuil(0.9)` renvoie 13. 


## <font color=orange>Métropole - 2021</font>
La suite $(T_n)$ est définie par $T_{n+1} = 0,94T_n + 1,5$.

Le programme suivant, écrit en langage Python, doit renvoyer après son exécution la plus petite valeur de l’entier $n$ pour laquelle $T_n > 10$.

{{ IDE('./2-metropole2021/metropole2021', MAX = 5, SANS = 'max,min') }}
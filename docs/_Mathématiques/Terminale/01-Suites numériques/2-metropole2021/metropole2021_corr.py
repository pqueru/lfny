def seuil():
    n = 0
    T = -19 # On initialise la valeur de T_0
    while T < 10 : # Tant que l'objectif n'est pas atteint, on continue
        T = 0.94 * T + 1.5 # On calcule le terme suivant
        n = n + 1
    return n # On renvoie la plus petite valeur de n pour laquelle l'objectif est atteint
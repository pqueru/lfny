# Fonctions

## <font color=orange>Nouvelle Calédonie - 2021</font>
On considère la fonction $f$ définie pour tout réel x par : $f(x) = x^3 e^{x}$.

On considère la fonction `fonc`, écrite en langage Python ci-dessous.

{{ IDE('./caledonie2021/caledonie2021') }}

Déterminer, sans justifier, la valeur renvoyée par `fonc(2)` arrondie à $10^{−3}$.

??? "Solution"    
    - `fonc(2)` renvoie $-0,034$ arrondie à $10^{−3}$.

## <font color=orange>Madagascar - 2022</font>
On considère la fonction $f$ définie pour tout réel x par : $f(x) = 1 + x − e^{0,5x−2}$.

On considère la suite $(u_n)$ définie par $u_0 = 0$ et, pour tout entier naturel $n$, $u_{n+1} = f(u_n)$

On considère enfin la fonction `valeur` écrite ci-dessous dans le langage Python.

L'instruction `valeur(3.99)` renvoie la valeur `12`. 

Interpréter ce résultat dans le contexte de l'exercice.

{{ IDE('./1-madagascar2022/madagascar2022') }}

??? "Solution"    
    - Le plus petit entier $n$ pour lequel $u_n > 3,99$ est 12.
#--- HDR ---#
"""
import de la fonction exponentielle
"""
from math import exp
#--- HDR ---#

def valeur(a):
    u = 0
    n = 0
    while u <= a :
        u = 1 + u - exp(0.5*u - 2)
        n = n + 1
    return n
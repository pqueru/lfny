#--- HDR ---#
"""
import de la fonction exponentielle
"""
from math import exp
#--- HDR ---#

def fonc(n):
    u = -1
    for i in range(n):
        u = u**3 * exp(u)
    return u
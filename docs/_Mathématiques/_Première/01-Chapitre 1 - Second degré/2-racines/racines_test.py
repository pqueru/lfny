assert racines(-2, 1, -1) == []
assert racines(2, 1, -1) == [-1.0, 0.5]
assert racines(1, 2, 1) == [-1.0]
# Chapitre 2 : Suites

!!! example "Exemples"
    === "Question 1."
        Que fait la fonction suivante ?
        ```python
        def ma_suite(n):
            i = 1
            u = 1
            while i < n:
                i = i + 1
                u = u + i
            return u
        ```
        ??? done "Solution"
            Elle calcule le terme de rang n de la suite définie par récurrence par $u_0 = 1$ et $u_{n} = u_{n-1} + n$.
    === "Questions 2."
        Écrire un script permettant d'afficher les termes 1 à 10 de la suite :
        {{ IDE('0-ma_suite_affichage/ma_suite_affichage') }}
        ??? "Solution"
            ```python
            for i in range(10):
                print(ma_suite(i + 1))
            ```

    === "Solution"
        ```python
        for i in range(10):
            print(ma_suite(i + 1))
        ```
        ou
        ```python
        for i in range(1, 11):
            print(ma_suite(i))
        ```

### Liste des `n` premiers termes d'une suite
Écrire une fonction `table` prenant un entier `n` en paramètre et renvoyant la liste des n premiers termes de la suite :
{{ IDE('./1-ma_suite_table/ma_suite_table') }}

### Proposez une liste en compréhension pour créer la liste précédente.
{{ IDE('./2-ma_suite_comprehension/ma_suite_comprehension') }}

??? "Solution"
    ```python
    t = [ma_suite(i) for i in range(10)]
    print(t)
    ```

## <font color=green>Calcul du n-ième terme d'une suite définie par récurrence</font>
Écrire une fonction `calcul_terme` qui prend en paramètres un entier `n` et qui renvoie la valeur du terme de rang $n$ de la suite définie par
$\left\{
    \begin{array}{l}
        u_0 = 1 \\
        u_{n+1} = \dfrac{u_n - 1}{u_n - 2}
    \end{array}
\right.$

{{ IDE('./3-calcul_terme/calcul_terme', MAX = 10, SANS = 'max,min') }}

## <font color=green>Seuil</font>
Écrire une fonction `seuil` qui prend en paramètres un nombre `x` et qui détermine le premier entier à partir duquel la suite définie par :
$\left\{
    \begin{array}{l}
        u_0 = 1~000 \\
        u_{n+1} = 1,2 u_n - 100
    \end{array}
\right.$ est supérieure ou égale à $x$.

{{ IDE('./4-seuil/seuil', MAX = 10, SANS = 'max,min') }}


## <font color=green>Somme</font>
Écrire une fonction `somme` qui prend en paramètre un entier `n` et qui calcule la somme $S_n = 1 + 2 + \dots + n$

{{ IDE('./5-somme/somme', MAX = 10, SANS = 'max,min') }}

## Suite de Syracuse

!!! note "Calcul d'un terme"
    La suite de Syracuse est définie par $u_0 \in \mathbb{R}$ et pour tout $n \geq 1$, on a :        
    $\left\{
    \begin{array}{l}
        u_{n+1} = \dfrac{u_n}{2} \text{ si } u_n \text{ est pair.}\\
        u_{n+1} = 3u_n + 1 \text{ si } u_n \text{ est impair.}
    \end{array}
    \right.$

    Écrire une fonction `suivant_syracuse` qui prend en paramètre un nombre entier et qui renvoie le terme suivant de la suite de Syracuse.
    {{ IDE('./6-suivant_syracuse/suivant_syracuse', MAX = 10, SANS = 'count') }}

!!! note "Liste des termes"
    Écrire une fonction `liste_syracuse` qui prend en paramètre un entier `n` et qui renvoie la liste des termes jusqu'à obtenir 1 et le temps de vol.
    {{ IDE('./7-liste_syracuse/liste_syracuse', MAX = 10, SANS = 'count') }}

## Suite de Fibonacci

La suite de Fibonacci est la suite définie par $f_0=0$, $f_1=1$ et pour tout $n \geq 2$, $f_n=f_{n-1}+f_{n-2}$  

Écrire une fonction `fibonacci` qui prend en paramètre un entier `n` et qui renvoie la liste des $n+1$ premiers termes de la suite de Fibonacci.
{{ IDE('./8-fibonacci/fibonacci', MAX = 10, SANS = 'count') }}

def seuil(x):
    n = 0
    u = 1000
    while u < x:
        u = u * 1.2 - 100
        n = n + 1
    return n
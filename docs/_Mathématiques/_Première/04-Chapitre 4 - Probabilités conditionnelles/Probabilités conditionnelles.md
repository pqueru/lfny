# Chapitre 4 : Probabilités conditionnelles

## <font color=green>Méthode de Monte-Carlo</font>

Pour calculer une surface, le principe est toujours le même :
- On choisit une surface simple (dont on connaît l’aire) qui englobe la surface à calculer.
- On choisit aléatoirement plusieurs points dans cette surface simple, et pour chacun d’eux on
d´etermine s’il est à l’intérieur de la surface à calculer.
- Le ratio entre le nombre de points intérieur et le nombre de points total donne une approximation
du nombre recherché.

$\text{surface à calculer} = \text{surface totale} \times \dfrac{N_{internes}}{N_{total}}$


Cela pose quelques contraintes :  
- Peut-on trouver une surface (simple) englobant la surface recherchée ?  
- Peut-on savoir si un point est à l’intérieur de cette surface ?  
- Peut-on effectuer un tirage ´equiprobable dans la surface englobante ?  

!!! info "Rappels"
    La fonction `randint` permet d'obtenir un entier aléatoire entre deux entiers **inclus**.
    
    On peut obtenir de l'aide sur une fonction en tapant :
    ```python
    >>> help(randint)
    Help on method randint in module random:

    randint(a, b) method of random.Random instance
        Return random integer in range [a, b], including both end points.
    ```

    La fonction `random` permet d'obtenir un réel aléatoire entre **0 inclus** et **1 exclu**.

    ```python
    Help on built-in function random:

    random() method of random.Random instance
        random() -> x in the interval [0, 1).
    ```

## Parabole
On considère la figure suivante :
![parabole](./IMG/parabole.png){width=150}

Écrire une fonction `calcul_aire_parabole` qui prend en paramètre la taille de l'echantillon et qui  calcule l'aire de la surface bleue en utilisant la méthode ci-dessus.

{{ IDE('./Monte-Carlo/calcul_aire_parabole') }}

## Disque
On considère la figure suivante :
![parabole](./IMG/disque.png){width=150}

Écrire une fonction `calcul_aire_disque` qui prend en paramètre la taille de l'echantillon et qui calcule l'aire de la surface bleue.

{{ IDE('./Monte-Carlo/calcul_aire_disque') }}
# Variables

## <font color=orange>Exercice 1</font>
Affecter à $y$ la valeur $3x-2$.

{{ IDE('../Exercices/affectation1/affectation1', MAX = 5, SANS = 'max,min') }}

## <font color=orange>Exercice 2</font>
Affecter à $y$ la valeur $2x^2 - 3x + \dfrac{1}{2}$.

{{ IDE('../Exercices/affectation2/affectation2', MAX = 5, SANS = 'max,min') }}

## <font color=orange>Exercice 3</font>
Écrire une fonction `affectation` qui prend en paramètre un nombre $x$, qui affecte à $y$ la valeur $\dfrac{x-2}{x+1}$ et qui renvoie cette valeur.

{{ IDE('../Exercices/affectation3/affectation3', MAX = 5, SANS = 'max,min') }}
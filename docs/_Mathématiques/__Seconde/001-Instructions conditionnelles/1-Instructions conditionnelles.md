# Instructions conditionnelles

## If

Compléter la fonction `nouveau_prix` qui renvoie le prix réduit de 5% si ce prix est supérieur ou égal à 100.

{{ IDE('../Exercices/if/if', MAX = 5, SANS = 'max,min') }}

## If Else

Complétez la fonction `maximum` qui envoie le plus grand des deux nombres a et b.

{{ IDE('../Exercices/if/if_else_maximum', MAX = 5, SANS = 'max,min') }}

## If Elif Else

Complétez la fonction `tarif` pour qu'elle renvoie la chaîne de caractères :

- "plein"   lorsque `age` est supérieur ou égal à 18

- "reduit"   lorsque `age` est compris entre 12 inclus et 18 exclu

- "gratuit"   lorsque `age` est inférieur à 12

{{ IDE('../Exercices/if/if_elif_else_age', MAX = 5, SANS = 'max,min') }}
# Boucle for (boucle bornée)

## Nombres successifs
Modifier la fonction `affiche_10_15` qui affiche les nombres entiers de 10 à 15 inclus, chacun sur une ligne.

{{ IDE('../Exercices/for/affiche_10_15', MAX = 5, SANS = 'max,min') }}

## Table de multiplication
Modifiez le programme suivant pour qu'il affiche la table de multiplication de 5, pour les nombres de 0 à 10 inclus.

5 * 0 = 0  
5 * 1 = 5  
...  
5 * 10 = 50

{{ IDE('../Exercices/for/table_5', MAX = 5, SANS = 'max,min') }}

## Compteur
Compléter la fonction `compteur` qui prend en paramètre une chaîne de caractères `mot` et qui renvoie le nombre de lettres de ce mot. On n'utilisera pas la fonction `len`.

{{ IDE('../Exercices/for/compteur', MAX = 5, SANS = 'max,min,len') }}

## Compter lettre
Compléter la fonction `compter_lettre` qui prend en paramètre une lettre `lettre` et une chaîne de caractères `phrase` et qui renvoie le nombre d'occurences (le nombre d'apparition) de la lettre `lettre` dans la chaîne de caractères `phrase`.

{{ IDE('../Exercices/for/compter_lettre', MAX = 5, SANS = 'max,min') }}

## Compter lettre - version 2
Compléter la fonction `compter_lettre_v2` qui prend en paramètre une lettre `lettre` et une chaîne de caractères `phrase` et qui renvoie le nombre d'occurences (le nombre d'apparition) de la lettre `lettre` dans la chaîne de caractères `phrase`.

{{ IDE('../Exercices/for/compter_lettre_v2', MAX = 5, SANS = 'max,min') }}
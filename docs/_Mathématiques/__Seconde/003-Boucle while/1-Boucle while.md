# Boucle while (boucle non bornée)

## Nombres
Modifier le programme suivant pour qu'il s'arrête dès que $n$ est supérieur ou égal à 1000.

{{ IDE('../Exercices/while/nombre', MAX = 5, SANS = 'max,min') }}

??? done "Solution"
    ```python hl_lines="2"
    def nombre(n):
        while n <= 1000 :
            n = n + 1

        print("Le programme s'est terminé avec n =", n)
    ```

## Inéquation
Modifier la fonction `resolution_inequation` qui renvoie le plus petit entier positif $n$ telle que $n^2 > 30(n + 55)$.

{{ IDE('../Exercices/while/inequation', MAX = 5, SANS = 'max,min') }}

??? done "Solution"
    ```python hl_lines="4-5"
    def inequation():
        n = 0
        
        while n**2 <= 30 * (n + 55):
            n = n + 1

        return n
    ```
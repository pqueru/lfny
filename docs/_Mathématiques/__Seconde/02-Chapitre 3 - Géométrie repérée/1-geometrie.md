# Chapitre 3 : Géométrie repérée

## Triangle rectangle
### Version facile
Écrire une fonction `est_triangle_rectangle` prenant en paramètres les longueurs des trois côtés d'un triangle et renvoyant `True` si le triangle est rectangle et `False` sinon.

On supposera que les longueurs des côtés sont données dans l'ordre croissant et que ces longueurs sont des entiers.

{{ IDE('../Exercices/est_triangle_rectangle/est_triangle_rectangle_v1', MAX = 5, SANS = 'max,min') }}

### Version difficile
Écrire une fonction `est_triangle_rectangle` prenant en paramètres les longueurs des trois côtés d'un triangle et renvoyant `True` si le triangle est rectangle et `False` sinon.

On ne supposera pas que les longueurs des côtés sont données dans l'ordre croissant et que ces longueurs sont des entiers.

{{ IDE('../Exercices/est_triangle_rectangle/est_triangle_rectangle', MAX = 5, SANS = 'max,min') }}

??? tip "Aide 1"
    Penser à déterminer la longueur du plus grand côté.

??? tip "Aide 2"
    Penser à tester l'écart entre la somme des carrés des longueurs des deux plus petits côtés et le carré de la longueur du plus grand et non directement l'égalité.

## Coordonnées du milieu d'un segment
Écrire une fontion `coordonnees_milieu` qui prend en paramètre les coordonnées de deux points A et B et qui renvoie le couple de coordonnées du mileu du segment [AB].

{{ IDE('../Exercices/coordonnees_milieu/coordonnees_milieu', MAX = 5, SANS = 'max,min') }}

## Coordonnées d'un vecteur
Écrire une fontion `coordonnees_AB` qui prend en paramètre les coordonnées de deux points A et B et qui renvoie le couple de coordonnées du vecteur $\vec{AB}$.

{{ IDE('../Exercices/coordonnees_AB/coordonnees_AB', MAX = 5, SANS = 'max,min') }}

## Parallélogramme ?
Écrire une fontion `est_parallelogramme` qui prend en paramètre les coordonnées de quatre points A, B, C et D et qui renvoie `True` si ABCD est un parallélogramme.

On pourra utiliser la fonction `coordonnees_AB` ou la fonction `coordonnees_milieu`.

{{ IDE('../Exercices/est_parallelogramme/est_parallelogramme', MAX = 5, SANS = 'max,min') }}

## Distance entre deux points
Écrire une fontion `distance` qui prend en paramètre les coordonnées de deux points A et B et qui renvoie la longueur du segment [AB].

{{ IDE('../Exercices/distance/distance', MAX = 5, SANS = 'max,min') }}


def coordonnees_milieu(xa, ya, xb, yb):
    xi = (xa + xb) / 2
    yi = (ya + yb) / 2
    return (xi, yi)
from math import sqrt

def distance(xA, yA, xB, yB):
    z = (xB - xA)**2 + (yB - yA)**2
    return sqrt(z)
def est_triangle_rectangle(a, b, c):
    if a > b:
        if a > c:
            return abs(b**2 + c**2 - a**2) < 10**-6
        else:
            return abs(a**2 + b**2 - c**2) < 10**-6
    else:
        if b > c:
            return abs(a**2 + c**2 - b**2) < 10**-6
        else:
            return abs(a**2 + b**2 - c**2) < 10**-6
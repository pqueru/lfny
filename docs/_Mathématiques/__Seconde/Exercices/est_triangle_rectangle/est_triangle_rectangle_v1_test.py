assert est_triangle_rectangle(3, 4, 5) == True
assert est_triangle_rectangle(5, 3, 4) == True
assert est_triangle_rectangle(24, 25, 7) == True
assert est_triangle_rectangle(2, 3, 4) == False
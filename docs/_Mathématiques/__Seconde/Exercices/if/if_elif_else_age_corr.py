def tarif(age):
    if age >= 18:
        return "plein"
    elif age >= 12:
        return "reduit"
    else:
        return "gratuit"

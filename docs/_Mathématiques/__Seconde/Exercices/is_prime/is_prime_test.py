assert is_prime(1) == False
assert is_prime(2) == True
assert is_prime(3) == True
assert is_prime(4) == False
assert is_prime(97) == True
assert is_prime(525) == False
assert is_prime(7907) == True
# ⌨ Bonnes pratiques de codage

## Définitions

### PEP

PEP signifie _**P**ython **E**nhancement **P**roposal_, et il y en a plusieurs. Un PEP est un document qui décrit les nouvelles fonctionnalités proposées pour Python et documente les aspects de Python, comme la conception et le style, pour la communauté.

### *snake_case*

Le [*snake case*](https://fr.wikipedia.org/wiki/Snake_case){ target=_blank } est une convention typographique en informatique consistant à écrire des ensembles de mots, généralement, en minuscules en les séparant par des tirets bas.

> Exemple : `un_joli_nom_de_variable`

### *camel case*

Le *camel case* consiste à mettre en majuscule les premières lettres de chaque mot, sans séparer par un tiret bas.

> Exemple : `UnNomDeClasse`

## PEP 8

PEP 8, parfois orthographié PEP8 ou PEP-8, est un document qui fournit des directives et des meilleures pratiques sur la façon d'écrire du code Python. Il a été écrit en 2001 par Guido van Rossum (le créateur de Python), Barry Varsovie et Nick Coghlan. L'objectif principal de PEP 8 est d'améliorer la lisibilité et la cohérence du code Python.

Le poème "The zen of Python" écrit par Timp Peters résume la philosopie d'un bon programmeur en Python et est accessible par un simple import (surnommé *Easter Egg*)

```console
>>> import this

The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
```


### Les identifiants

1. On utilisera le *snake_case* pour toutes les variables, sauf :
    + pour les `class` que l'on nomme en *camel case*,
    + pour les constantes, en majuscule.

2. On donne des identifiants qui ont du sens.

=== "à éviter"
    ```python
    p = (5, 8)
    x, y = p
    ```

=== "à privilégier"
    ```python
    point_P = (5, 8)
    point_P_x, point_P_y = point_P
    ```

### Aération du code

- L'indentation doit être de **4** espaces.
- Une ligne vide après une définition de fonction.
- Deux lignes vides après une définition de classe.
- Une ligne vide pour séparer deux étapes marquées d'un groupe d'instructions.
- Une ligne vide pour aérer chaque *doctest*.
- Maximum 79 caractères de code par ligne.
    - Une ligne trop longue peut être coupée sans problème **à l'intérieur** de parenthèses, crochets ou accolades ; on parle de continuation implicite.
    - Sinon, on peut toujours couper une ligne avec `\`.
    - La continuation implicite est **à privilégier** dès que possible !

=== "continuation implicite"
    ```python 
    def ma_fonction(argument_1,
                    argument_2, argument_3, argument_4):
        pass
    ```

=== "ligne coupée avec \"
    ```python
    ma_fonction(22, 23, 24, 25) + \
        ma_fonction(34, 35, 36, 37)
    ```

### Aération des opérateurs

On met de l'espace autour des opérateurs binaires, sauf

- pour les paramètres par défaut des fonctions,
- pour les opérations prioritaires
- pour `:` dans les définitions de tranches

=== "Mal"
    ```python
    x = a*b+c
    if x > 5 and x % 2 == 0:
        print('x est plus grand que 5 et divisible par 2.')
    ```

=== "Moyen"
    ```python
    x = a * b + c
    if x>5 and x%2==0:
            print('x est plus grand que 5 et divisible par 2.')
    ```

=== "Bien"
    ```python
    x = a*b + c
    if (x > 5) and (x % 2 == 0):
            print('x est plus grand que 5 et divisible par 2.')
    ```

On met un espace :

- juste après une virgule

```python
mon_tuple = (4, 11, 25)
```

On **ne met pas** d'espace

- juste après une parenthèse ouvrante (ou crochet ou accolade)
- juste avant une parenthèse fermante (ou crochet ou accolade).

On ne place jamais d'espace en fin de ligne.

### Pour aller plus loin

Inspiré de ce [document](https://www.codeflow.site/fr/article/python-pep8){ target=_blank }, vous retrouverez plus de détails.


```python
# Non recommandée, avec une conversion implicite, mais juste
ma_liste = []
if not len(my_list):
    print('ma_liste est vide !')
```

En Python, toute liste, chaine ou tuple **vide** est convertie implicitement à `False`.

Il est donc possible de proposer une alternative plus simple à ce qui précède :

```python
# Correcte, mais contient aussi une conversion implicite
ma_liste = []
if not ma_liste:
    print('ma_liste est vide !')
```

Alors que les deux exemples afficheront que la liste est vide, la deuxième option est plus simple, donc PEP 8 l'encourage.

> ⚠️ Contrairement au PEP 8, nous préférons éviter la conversion implicite, donc **une autre méthode est recommandée**.

```python
# La bonne méthode
ma_liste = []

if ma_liste == []:
    print("ma_liste est vide !")
# ou alors
if ma_liste != []:
    print("ma_liste n'est pas vide !")
```
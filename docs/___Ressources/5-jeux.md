# 🎲 Jeux

[![Tours de Hanoi](https://www.natureetdecouvertes.com/fstrz/r/s/cache.natureetdecouvertes.com/Medias/Images/Articles/91028350/TG3153-2025_P1.jpg?width=750&height=750&frz-v=284"/){width=250}](/lfny/__NSI/Terminale%20NSI/Tours_Hanoi.html){target=_blank}

[Silent teacher](https://silentteacher.toxicode.fr/){target=_blank} - Pour des bases de programmation (débutant)

##### 🏴‍☠️ pyRATES - un jeu pour revoir les bases de Python
[![pyRATES](../images/pyrates.png){width=250}](https://py-rates.fr/){target=_blank}